import stomp
import time
import sys
import pymysql

user = "admin"
password = "admin"
host = "10.0.0.3"
port = 61613
#destination = "/topic/event"
destination = "/queue/raceXevent"

class MyListener(object):
  
  def __init__(self, conn):
    self.conn = conn
    self.count = 0
    self.start = time.time()
  
  def on_error(self, headers, message):
    print('received an error %s' % message)

  def persistMessage(self):
    print("Persisting Payload")

  def on_message(self, headers, message):      
    print("Received " + ' ' + str(message))          
#    persistMessage()
    try:
        with dBconnection.cursor() as cursor:
            sql = "INSERT INTO `speedracex`.`raw_events` (`created_date`, `payload` ,`processed_date`) VALUES (%s, %s ,%s);"
            try:
                cursor.execute(sql, ("2018-10-02", message, "2018-10-02"))
                print("Event added successfully")
            except:
                print("Oops! Something wrong")
 
        dBconnection.commit()
    finally:
        dBconnection.close()
#        print("Closing dBconnection")

# Main

dBconnection = pymysql.connect(
    host='10.0.0.9',
    user='craigm',
    password='devuser',
    db='speedracex',
)

conn = stomp.Connection(host_and_ports = [(host, port)])
conn.set_listener('', MyListener(conn))
conn.start()
conn.connect(login=user,passcode=password)
conn.subscribe(destination=destination, id=1, ack='auto')
print("Waiting for messages...")


while 1: 
  time.sleep(10) 
