import mysensors.mysensors as mysensors
import binascii
#import struct
from struct import *
import stomp
import time
import sys

user = "admin"
password = "admin"
host = "localhost"
port = 61613
#destination = "/topic/event"
destination = "/queue/event"
#destination = destination[0]

messages = 10000


# Resource for bye formatting and parsing
# https://www.devdungeon.com/content/working-binary-data-python

def processEventPacket(payload):  # 0106BDA65BF401
    # Parsing processEventPacket
    binary_data = binascii.unhexlify(payload)
    payloadArray = bytearray(binary_data)
    #print('Raw Payload' + binary_data)
    # Given raw bytes, get an ASCII string representing the hex values
    hex_data = binascii.hexlify(binary_data)  # Two bytes values 0 and 255
    # The resulting value will be an ASCII string but it will be a bytes type
    # It may be necessary to decode it to a regular string
    text_string = hex_data.decode('utf-8')  # Result is string "00ff"
  #  print('Event Payload: ' + text_string + ' (' + str(binary_data) + ')' + ' Type: ' + str(payloadArray[0]))

    print('Event Payload: ' + text_string + ' Type: ' + str(payloadArray[0]))
    
     
    # For more information on format strings and endiannes, refer to
    # https://docs.python.org/3.5/library/struct.html
    # When unpacking, you receive a tuple of all data in the same order
    # https://docs.python.org/3.5/library/struct.html#format-characters
    #tuple_of_data = struct.unpack("BIH", binary_data)
    
    #unpack('b', 'A')
#    print ('4 byte (I): ' + str(unpack('I', b'\x00\x01\x00\x02')))
#    print ('4 byte (i): ' + str(unpack('i', b'\x00\x01\x00\x02')))
#    print ('2 byte (h): ' + str(unpack('h', b'\x00\x01')))    
#    print ('1 byte (c): ' + str(unpack('c', b'\x01')))
    
#    print ('byte Array: ' + str(bytes(bytearray(binary_data))) + 'Length: ' + str(len(binary_data)))
#   tuple_of_data = unpack('cih', hex_data)    # This does not work
#   tuple_of_data = unpack('cih', binary_data)
#   print(tuple_of_data)
    
    if(payloadArray[0] == 1):
#        print ('Payload Header: ' +str(payloadArray[0]))
        # Now lets pack the 4 byte time stamp
        sensorOffset =1
        timeOffset = 2  
        msOffset =6
        sensorId = payloadArray[sensorOffset]
        timeStamp_dataArray = bytes([payloadArray[timeOffset], payloadArray[timeOffset+1], payloadArray[timeOffset+2], payloadArray[timeOffset+3] ])
        timeStamp_ms_dataArray = bytes([payloadArray[msOffset], payloadArray[msOffset+1]])      
        ms = unpack('h', timeStamp_ms_dataArray)
        time = unpack('i', timeStamp_dataArray)
        print('sensorId: ' + str(sensorId) + ' Time Payload: ' + str(time[0]) + '.' + str(ms[0]))


def processMessage(message):
    # Message
    ##  gateway - the gateway instance
    ##  node_id - the sensor node identifier
    #   child_id - the child sensor id
    #   type - the message type (int)
    #   ack - True is message was an ACK, false otherwise
    #   sub_type - the message sub_type (int)
    #   payload - the payload of the message (string)    
    text = 'processMessage: (' + str(message.node_id) + ') MsgType: ' + str(message.type) + ' ChildId: ' + str(message.child_id) + ' sub_type: ' + str(message.sub_type) + ' Payload: ' + message.payload
    
    print('processMessage: (' + str(message.node_id) + ') MsgType: ' + str(message.type) + ' ChildId: ' + str(message.child_id) + ' sub_type: ' + str(message.sub_type) + ' Payload: ' + message.payload)     
    # Starting with a hex string you can unhexlify it to bytes
    
    if(message.sub_type == 48): # V_CUSTOM
        #rawBytes = binascii.unhexlify(message.payload)
        #print(rawBytes)
        # Given raw bytes, get an ASCII string representing the hex values
        #hex_data = binascii.hexlify(rawBytes)  # Two bytes values 0 and 255
        # The resulting value will be an ASCII string but it will be a bytes type
        # It may be necessary to decode it to a regular string
        #text_string = hex_data.decode('utf-8')  # Result is string "00ff"
        #print('Event Payload: ' + text_string)
        #conn.send(text, destination, persistent='false')
        conn.send(destination,message.payload, persistent='false')
        processEventPacket(message.payload)
    
    
    
    

def event(message):
    """Callback for mysensors updates."""
    # print('sensor_update (' + str(message.node_id) + ') Payload: ' + message.payload)     
    processMessage(message)
    

GATEWAY = mysensors.SerialGateway(
  '/dev/ttyUSB0', baud=38400, timeout=1.0, reconnect_timeout=10.0,
  event_callback=event, persistence=True,
  persistence_file='somefolder/mysensors.pickle', protocol_version='2.0')
GATEWAY.start()
conn = stomp.Connection(host_and_ports = [(host, port)])
conn.start()
conn.connect(login=user,passcode=password)
