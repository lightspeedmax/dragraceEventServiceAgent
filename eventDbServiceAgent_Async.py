import stomp
import time
import sys
import pymysql
import queue as Queue
from threading import Thread
import time
import datetime

user = "admin"
password = "admin"
host = "10.0.0.3"
port = 61613
#destination = "/topic/event"
destination = "/queue/raceXeventAchive"
message_queue = Queue.Queue()

class MyListener(object):
  
  def __init__(self, conn):
    self.conn = conn
    self.count = 0
    self.start = time.time()
  
  def on_error(self, headers, message):
    print('received an error %s' % message)

  def persistMessage(self):
    print("Persisting Payload")

  def on_message(self, headers, message):      
    print("Received " + ' ' + str(message))          
    message_queue.put(message)

# Main
def dB_queue(mQueue):
    """This is the worker thread function.
    It processes items in the queue one after
    another.  These daemon threads go into an
    infinite loop, and only exit when
    the main thread ends.
    """
    print("DB Queue Thread")
    while True:
        message = mQueue.get()
        print ('Message: ' + str(message))
        try:
            with dBconnection.cursor() as cursor:
                ts = time.time()
                timestamp = datetime.datetime.fromtimestamp(ts).strftime('%Y-%m-%d %H:%M:%S')
                sql = "INSERT INTO `speedracex`.`raw_events` (`created_date`, `payload` ,`processed_date`) VALUES (%s, %s ,%s);"
                try:
                    cursor.execute(sql, (timestamp, message, timestamp))
                    print("Event added successfully")
                except:
                    print("Oops! Something wrong")

            dBconnection.commit()
        finally:
            
            time.sleep(2)
            mQueue.task_done()


dBconnection = pymysql.connect(
    host='10.0.0.9',
    user='craigm',
    password='devuser',
    db='speedracex',
)

worker = Thread(target=dB_queue, args=(message_queue,))
worker.setDaemon(True)
worker.start()
conn = stomp.Connection(host_and_ports = [(host, port)])
conn.set_listener('', MyListener(conn))
conn.start()
conn.connect(login=user,passcode=password)
conn.subscribe(destination=destination, id=1, ack='auto')
print("Waiting for messages...")



while 1: 
  time.sleep(60) 
  message_queue.join()
  print ('***DB Agent Heartbeat')


dBconnection.close()
print("Closing dBconnection")
