import stomp
import time
import sys
#import pymysql
import queue as Queue
from threading import Thread
import time
import datetime
import binascii

SUBSCRIBE_TOPIC       = "ls/#" #ls/racex/out"
DESTINATION_QUEUE     = "/queue/raceXevent"
DESTINATION_ARCHIVE_QUEUE     = "/queue/raceXeventAchive"
BROKER_USERNAME  = "admin"
BROKER_PASWORD   = "admin"
BROKER_HOST_URL  = "localhost"
BROKER_HOST_PORT = 61613

MQTT_BROKER_HOST_URL  = "10.0.0.16"
MQTT_BROKER_HOST_PORT = 1883

user = "admin"
password = "admin"
host = "10.0.0.3"
port = 61613 #1883 
brokerport = 61613

#destination = "/topic/ls/racex/d2s" # "/queue/event"
destination = "/topic/ls.racex.d2s" 
#destination = "/topic/#"
message_queue = Queue.Queue()

class MyListener(object):
  
  def __init__(self, MQTTClient):
    self.MQTTClient = MQTTClient
    self.count = 0
    self.start = time.time()
  
  def on_error(self, headers, message):
    print('received an error %s' % message)

  def persistMessage(self):
    print("Persisting Payload")

  def on_message(self, headers, message):      
    binary_data = message.encode('utf-8')    
    #binary_data = bytes(message, 'utf-8')
    payloadArray = bytearray(binary_data)
    hex_data = binascii.hexlify(binary_data)
    text_string = hex_data.decode('utf-8')
    #print('Event Payload: ' + text_string)# + ' Type: ' + str(payloadArray))        
    message_queue.put(hex_data) #binary_data)

# Main
def dB_queue(mQueue):
    """This is the worker thread function.
    It processes items in the queue one after
    another.  These daemon threads go into an
    infinite loop, and only exit when
    the main thread ends.
    """
    print("Incoming Message Queue Thread Started")
    while True:
        message = mQueue.get()
        #print ('Message: ' + str(message))
        try:
            #with dBconnection.cursor() as cursor:
                ts = time.time()
                timestamp = datetime.datetime.fromtimestamp(ts).strftime('%Y-%m-%d %H:%M:%S')
                text_string = message.decode('utf-8')
                #sql = "INSERT INTO `speedracex`.`raw_events` (`created_date`, `payload` ,`processed_date`) #VALUES (%s, %s ,%s);"
                try:
                 #   cursor.execute(sql, (timestamp, message, timestamp))
                    print("Message: " + "(" + timestamp + ") 0x" + text_string) #Event added successfully")
                    MQTTClient.send(DESTINATION_QUEUE, message)
                    MQTTClient.send(DESTINATION_ARCHIVE_QUEUE, message)
                except:
                    print("Failed to Queue Message: " + "(" + timestamp + ") 0x" + text_string)
                    #print("Oops! Something wrong")

            #dBconnection.commit()
        finally:
            
            time.sleep(1)
            mQueue.task_done()

#======= Main ======================================

worker = Thread(target=dB_queue, args=(message_queue,))
worker.setDaemon(True)
worker.start()
MQTTClient = stomp.Connection(host_and_ports = [(host, port)])
MQTTClient.set_listener('', MyListener(MQTTClient))
MQTTClient.start()
MQTTClient.connect(login=user,passcode=password)
MQTTClient.subscribe(destination=destination, id=1, ack='auto')
print("Waiting for messages...")

#BrokerClient = stomp.Connection(host_and_ports = [(host, brokerport)])
#BrokerClient.start()
#BrokerClient.connect(login=user,passcode=password)




while 1: 
  time.sleep(60) 
  message_queue.join()
  print ('***MQTT Heartbeat')
