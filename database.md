

CREATE DATABASE speedracex;


CREATE TABLE IF NOT EXISTS events (
    event_id INT AUTO_INCREMENT,
    device_id INT NOT NULL,
    event_date DATE,
    event_type INT,
    description TEXT,
    updated_date DATE,
    PRIMARY KEY (event_id)
)  ENGINE=INNODB;


CREATE TABLE IF NOT EXISTS users (
    user_id INT AUTO_INCREMENT,
    name_avatar VARCHAR(50) NOT NULL,
    created_date DATE,    
    PRIMARY KEY (user_id)
)  ENGINE=INNODB;


CREATE TABLE IF NOT EXISTS race_events (
    race_id INT AUTO_INCREMENT,    
    event_date DATE,
    event_id_start INT NOT NULL,
    event_id_end INT,
    track_distance INT,
    user_id INT NOT NULL,
    description TEXT,
    updated_date DATE,
    PRIMARY KEY (race_id)
)  ENGINE=INNODB;

CREATE TABLE IF NOT EXISTS raw_events (
    id INT AUTO_INCREMENT,    
    created_date TIMESTAMP,    
    payload VARCHAR(255),
    processed_date TIMESTAMP,
    PRIMARY KEY (id)
)  ENGINE=INNODB;


CREATE TABLE IF NOT EXISTS config (
    config_id INT AUTO_INCREMENT,
    config_key INT NOT NULL,
    value VARCHAR(50) NOT NULL,
    created_date DATE,    
    PRIMARY KEY (config_id)
)  ENGINE=INNODB;
